package linda.test.old;

import linda.*;

/**
 * Test tryTake 
 * Test case:
 * 	=> tryTake quand il n'y a aucun élément correspondant au template => expected : null (1)
 *  => Ajouter des éléments (2)
 *  => tryTake quand il a des éléments correspondant au template => expected : [ 4 "5" ] (3)
 *  => tryTake avec le même template pour vérifier que l'élément a été enlevé => expected :  [ 4 "foo" ] (4)
 */

public class Test1_mae {

    public static void main(String[] a) {
                
        // final Linda linda = new linda.shm.CentralizedLinda();
    	final Linda linda = new linda.server.LindaClient("//localhost:4000/LindaServer");
                
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, String.class);
                Tuple res = linda.tryTake(motif);
                System.out.println("(1) Resultat:" + res);
                linda.debug("(1)");
            }
        }.start();
                
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Tuple t1 = new Tuple(4, 5);
                System.out.println("(2) write: " + t1);
                linda.write(t1);

                Tuple t11 = new Tuple(4, "5");
                System.out.println("(2) write: " + t11);
                linda.write(t11);

                Tuple t2 = new Tuple("hello", 15);
                System.out.println("(2) write: " + t2);
                linda.write(t2);

                Tuple t3 = new Tuple(4, "foo");
                System.out.println("(2) write: " + t3);
                linda.write(t3);
                
                Tuple t4 = new Tuple(5, "cinq");
                System.out.println("(2) write: " + t4);
                linda.write(t4);
                                
                linda.debug("(2)");

            }
        }.start();
        
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, String.class);
                Tuple res = linda.tryTake(motif);
                System.out.println("(3) Resultat: " + res);
                linda.debug("(3)");
            }
        }.start();
        
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, String.class);
                Tuple res = linda.tryTake(motif);
                System.out.println("(4) Resultat: " + res);
                linda.debug("(4)");
            }
        }.start();
                
    }
}
