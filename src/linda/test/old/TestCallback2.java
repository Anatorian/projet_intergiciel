
package linda.test.old;

import linda.*;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;

/**
 * Callback TAKE avec déclenchement futur
 * Résultat attendu : CB got [ 5 "foo" ] => Si [4 "foo"], il a pris en compte l'existant, ce n'est pas bon
 * @author n7student
 *
 */

public class TestCallback2 {

    private static Linda linda;
    private static Tuple cbmotif;
    
    private static class MyCallback implements Callback {
        public void call(Tuple t) {
            System.out.println("CB got "+t);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            System.out.println("CB done with "+t);
        }
    }

    public static void main(String[] a) {
        // linda = new linda.shm.CentralizedLinda();
    	linda = new linda.server.LindaClient("//localhost:4000/LindaServer");

        Tuple t1 = new Tuple(4, 5);
        System.out.println("(2) write: " + t1);
        linda.write(t1);

        Tuple t2 = new Tuple("hello", 15);
        System.out.println("(2) write: " + t2);
        linda.write(t2);
        linda.debug("(2)");

        Tuple t3 = new Tuple(4, "foo");
        System.out.println("(2) write: " + t3);
        linda.write(t3);
        
        cbmotif = new Tuple(Integer.class, String.class);
        linda.eventRegister(eventMode.TAKE, eventTiming.FUTURE, cbmotif, new MyCallback());
        
        Tuple t4 = new Tuple(5, "foo");
        System.out.println("(2) write: " + t4);
        linda.write(t4);

        Tuple t5 = new Tuple("hola", 15);
        System.out.println("(2) write: " + t5);
        linda.write(t5);
        linda.debug("(2)");


    }

}
