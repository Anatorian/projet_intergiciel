package linda.test.old;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Test readAll
 */

import linda.*;

public class Test4 {

    public static void main(String[] a) {
                
        // final Linda linda = new linda.shm.CentralizedLinda();
    	final Linda linda = new linda.server.LindaClient("//localhost:4000/LindaServer");
                
        new Thread(() -> {
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Tuple motif = new Tuple(Integer.class, String.class);
            Collection<Tuple> res =  new LinkedList<Tuple>();
            res = linda.readAll(motif);
            System.out.println("(1) Resultat:" + res);
            linda.debug("(1)");
        }).start();
                
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Tuple t1 = new Tuple(4, 5);
                System.out.println("(2) write: " + t1);
                linda.write(t1);

                Tuple t11 = new Tuple(4, "5");
                System.out.println("(2) write: " + t11);
                linda.write(t11);

                Tuple t2 = new Tuple("hello", 15);
                System.out.println("(2) write: " + t2);
                linda.write(t2);

                Tuple t3 = new Tuple(4, "foo");
                System.out.println("(2) write: " + t3);
                linda.write(t3);
                
                Tuple t4 = new Tuple(5, "cinq");
                System.out.println("(2) write: " + t4);
                linda.write(t4);
                                
                linda.debug("(2)");

            }
        }.start();
        
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, String.class);
                Collection<Tuple> res =  new LinkedList<Tuple>();
                res = linda.readAll(motif);
                System.out.println("(3) Resultat:" + res);
                linda.debug("(3)");
            }
        }.start();
        
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, String.class);
                Collection<Tuple> res =  new LinkedList<Tuple>();
                res = linda.readAll(motif);
                System.out.println("(4) Resultat:" + res);
                linda.debug("(4)");
            }
        }.start();
                
    }
}
