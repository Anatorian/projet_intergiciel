package linda.test.test_runnable;

import linda.Linda;
import linda.Tuple;

import java.util.List;

public class RunnableFillLinda implements Runnable{

    private Linda linda;
    private List<Tuple> fill;
    private int order;
    private int waitingTime;
    private boolean verbose;

    public RunnableFillLinda(Linda linda, List<Tuple> fill, int waitingTime, int order, boolean verbose) {
        this.linda = linda;
        this.fill = fill;
        this.waitingTime = waitingTime;
        this.order = order;
        this.verbose = verbose;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(waitingTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (Tuple t: fill){
            if (verbose) {
                System.out.println("(" + order + ") Write : " + t);
            }
            linda.write(t);
        }
    }
}
