package linda.test.test_runnable;

import linda.Linda;
import linda.Tuple;

import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

public class RunnableTakeVerifyWait implements Runnable {

    private Linda linda;
    private Tuple motif;
    private int waitingTime;
    private int order;
    private List<Integer> resultList;
    private boolean verbose;

    public RunnableTakeVerifyWait(Linda linda, Tuple motif, int waitingTime, int order, boolean verbose){
        this.linda = linda;
        this.motif = motif;
        this.verbose = verbose;
        this.resultList = new LinkedList<>();
        this.waitingTime = waitingTime;
        this.order = order;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(waitingTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LocalTime t1 = LocalTime.now();
        linda.take(motif);
        LocalTime t2 = LocalTime.now();
        int delta = (t2.getSecond() - t1.getSecond())*1000;
        if (verbose) {
            System.out.println("(" + order + ") Take time : " + delta);
        }
        resultList.add(delta);

    }

    public List<Integer> getResultList() {
        return resultList;
    }
}
