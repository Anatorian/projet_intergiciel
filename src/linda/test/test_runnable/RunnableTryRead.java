package linda.test.test_runnable;

import linda.Linda;
import linda.Tuple;

import java.util.LinkedList;
import java.util.List;

public class RunnableTryRead implements Runnable{
    private Linda linda;
    private Tuple motif;
    private int waitingTime;
    private int order;
    private List<Tuple> resultList;
    private boolean verbose;

    public RunnableTryRead(Linda linda, Tuple motif, int waitingTime, int order, boolean verbose){
        this.linda = linda;
        this.motif = motif;
        this.verbose = verbose;
        this.resultList = new LinkedList<>();
        this.waitingTime = waitingTime;
        this.order = order;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(waitingTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Tuple res = linda.tryRead(motif);
        if (verbose) {
            System.out.println("(" + order + ") TryRead : " + res);
        }
        resultList.add(res);

    }

    public List<Tuple> getResultList() {
        return resultList;
    }
}
