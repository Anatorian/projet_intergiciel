package linda.test.test_runnable;

import linda.Callback;
import linda.Linda;
import linda.Tuple;

import java.util.LinkedList;
import java.util.List;

public class RunnableCB implements Runnable {
    private Linda linda;
    private Tuple motif;
    private int waitingTime;
    private int order;
    private List<Tuple> resultList;
    private boolean verbose;
    private Linda.eventMode eventMode;
    private Linda.eventTiming eventTiming;
    private boolean reinscription;

    public RunnableCB(Linda linda, Tuple motif, int waitingTime, int order, boolean verbose, Linda.eventMode eventMode, Linda.eventTiming eventTiming, boolean reinscription){
        this.linda = linda;
        this.motif = motif;
        this.verbose = verbose;
        this.eventMode = eventMode;
        this.eventTiming = eventTiming;
        this.reinscription = reinscription;
        this.resultList = new LinkedList<>();
        this.waitingTime = waitingTime;
        this.order = order;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(waitingTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        linda.eventRegister(eventMode, eventTiming, motif, new CallbackImpl());
    }

    public List<Tuple> getResultList() {
        return resultList;
    }

    private class CallbackImpl implements Callback{

        @Override
        public void call(Tuple t) {
            resultList.add(t);
            if (verbose) {
                System.out.println("(" + order + ") Callback got : " + t);
            }
            if (reinscription){
                linda.eventRegister(eventMode, eventTiming, motif, this);
            }
        }
    }
}
