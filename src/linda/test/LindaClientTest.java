package linda.test;

import junit.framework.TestCase;
import linda.Linda;
import linda.Tuple;
import linda.server.LindaClient;
import linda.server.LindaServer;
import linda.server.miscellaneous.DataSave;
import linda.server.miscellaneous.ServerState;
import linda.server.remote_server.Logger;
import linda.test.test_runnable.*;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static linda.server.LindaServer.SAVING_TIME;

public class LindaClientTest extends TestCase {

    private LindaClient linda;
    private boolean verbose = false;
    // Time between two saves of the server state

    public void setUp() throws Exception {
        super.setUp();
        LindaServer.main(new String[]{"false"});
        linda = new LindaClient("//localhost:4000/LindaServer");
    }

    public void tearDown() {
        LindaServer.kill();
        File f = new File("./saves/serverState.save");
        f.delete();
        linda = null;
    }

    public void testWrite() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Write test: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableTake rTake = new RunnableTake(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 1000, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertEquals(new Tuple(4, "foo"), rTake.getResultList().get(0));
    }

    public void testWriteVerifySideEffectOnTuple() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test write side effect: ");
        }

        List<Tuple> fill = new LinkedList<>();
        Tuple tTest = new Tuple(4, "foo");

        // Create fill list to fill the linda server
        {
            fill.add(tTest);
        }

        // Create needed Runnables
        RunnableTake rTake = new RunnableTake(this.linda, new Tuple(Integer.class, String.class), 10, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t2.join();
            // Change the tuple to verify we use a cloned value
            tTest.remove(0);
            t1.join();
        }

        // Verify properties
        assertEquals(new Tuple(4, "foo"), rTake.getResultList().get(0));
    }

    public void testTake() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test take: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableTake rTake = new RunnableTake(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertEquals("Message list should be empty", new LinkedList<Tuple>(), linda.getTuples());
        assertEquals(new Tuple(4, "foo"), rTake.getResultList().get(0));
    }

    public void testTakeMatchTemplate() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test take, verify it matches the template: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple("foo", "foo"));
            fill.add(new Tuple(4, "foo"));
            fill.add(new Tuple(5, "foo"));
        }

        // Create needed Runnables
        RunnableTake rTake = new RunnableTake(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        fill.remove(1);
        // Verify properties
        assertEquals(fill, linda.getTuples());
        assertEquals(new Tuple(4, "foo"), rTake.getResultList().get(0));
    }

    public void testTakeVerifyBlock() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test take verify that take is blocking until a tuple with the right template appears: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableTakeVerifyWait rTake = new RunnableTakeVerifyWait(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 5000, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertTrue("Time should be higher than 5000ms", 4000 <= rTake.getResultList().get(0));
    }

    public void testTakeNotDoubleDelete() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test take no double deletion: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableTake rTake = new RunnableTake(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        fill.remove(0);
        assertEquals("Message list should not be empty", fill, linda.getTuples());
    }

    public void testRead() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test Read: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableRead rRead = new RunnableRead(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rRead);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertEquals("Message list should not be empty", fill, linda.getTuples());
        assertEquals(new Tuple(4, "foo"), rRead.getResultList().get(0));
    }

    public void testReadMatchTemplate() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test Read: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple("foo", "foo"));
            fill.add(new Tuple(4, "foo"));
            fill.add(new Tuple(5, "foo"));
        }

        // Create needed Runnables
        RunnableRead rRead = new RunnableRead(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rRead);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertEquals("Message list should not have changed", fill, linda.getTuples());
        assertEquals(new Tuple(4, "foo"), rRead.getResultList().get(0));
    }

    public void testReadVerifyBlock() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test read verify that take is blocking until a tuple with the right template appears: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableReadVerifyWait rRead = new RunnableReadVerifyWait(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 4000, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rRead);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertTrue("Time should be higher than 5000ms", 4000 <= rRead.getResultList().get(0));
    }

    public void testReadVerifySideEffectOnTuple() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test Read side effect: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableRead rRead = new RunnableRead(this.linda, new Tuple(Integer.class, String.class), 1, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rRead);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t2.join();
            t1.join();
        }

        // Get the read tuple, modify it and reread it
        Tuple tTest = rRead.getResultList().get(0);
        tTest.remove(0);
        t1 = new Thread(rRead);
        t1.start();
        t1.join();

        // Verify properties
        assertEquals(new Tuple(4, "foo"), rRead.getResultList().get(1));
    }

    public void testTryTake() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test Trytake: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableTryTake rTry = new RunnableTryTake(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 100, 1, verbose);
        RunnableTryTake rTry2 = new RunnableTryTake(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTry);
        Thread t2 = new Thread(rFill);
        Thread t3 = new Thread(rTry2);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }
        // Wait for the fill to be end to launch the second tryTake
        t3.start();
        t3.join();

        // Verify properties
        List<Tuple> tList = new LinkedList<>();
        tList.add(null);
        assertEquals("Message list should be null", new LinkedList<Tuple>(), linda.getTuples());
        assertEquals("First tryTake should not have any result", tList, rTry.getResultList());
        assertEquals("Second tryTake should have a result", new Tuple(4, "foo"), rTry2.getResultList().get(0));
    }

    public void testTryTakeNotDoubleDelete() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test trytake no double deletion: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableTryTake rTake = new RunnableTryTake(this.linda, new Tuple(Integer.class, String.class), 20, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        fill.remove(0);
        assertEquals("Message list should not be empty", fill, linda.getTuples());
    }

    public void testTryTakeVerifyNonBlock() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test trytake verify that trytake is non blocking until a tuple with the right template appears: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableTryTakeVerifyWait rTake = new RunnableTryTakeVerifyWait(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 500, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertTrue("Time should be lower than 500ms", 500 >= rTake.getResultList().get(0));
    }

    public void testTryRead() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test Tryread: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableTryRead rTry = new RunnableTryRead(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 100, 1, verbose);
        RunnableTryRead rTry2 = new RunnableTryRead(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTry);
        Thread t2 = new Thread(rFill);
        Thread t3 = new Thread(rTry2);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }
        // Wait for the fill to be end to launch the second tryTake
        t3.start();
        t3.join();

        // Verify properties
        List<Tuple> tList = new LinkedList<>();
        tList.add(null);
        assertEquals("Message list should be null", fill, linda.getTuples());
        assertEquals("First tryTake should not have any result", tList, rTry.getResultList());
        assertEquals("Second tryTake should have a result", new Tuple(4, "foo"), rTry2.getResultList().get(0));
    }

    public void testTryReadVerifyNonBlock() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test tryread verify that tryread is non blocking until a tuple with the right template appears: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableTryReadVerifyWait rRead = new RunnableTryReadVerifyWait(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 500, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rRead);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertTrue("Time should be lower than 500ms", 500 >= rRead.getResultList().get(0));
    }

    public void testTakeAll() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test takeAll, verify it matches the template: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple("foo", "foo"));
            fill.add(new Tuple(4, "foo"));
            fill.add(new Tuple(5, "foo"));
        }

        // Create needed Runnables
        RunnableTakeAll rTry = new RunnableTakeAll(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 100, 1, verbose);
        RunnableTakeAll rTry2 = new RunnableTakeAll(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTry);
        Thread t2 = new Thread(rFill);
        Thread t3 = new Thread(rTry2);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }
        // Wait for the fill to be end to launch the second tryTake
        t3.start();
        t3.join();

        fill.remove(1);
        fill.remove(1);

        List<Tuple> tupleList = new ArrayList<>();
        tupleList.add(new Tuple(4, "foo"));
        tupleList.add(new Tuple(5, "foo"));
        // Verify properties
        assertEquals(fill, linda.getTuples());
        assertEquals(new LinkedList<Tuple>(), rTry.getResultList());
        assertEquals(tupleList, rTry2.getResultList());
    }

    public void testReadAll() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("Test ReadAll, verify it matches the template: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple("foo", "foo"));
            fill.add(new Tuple(4, "foo"));
            fill.add(new Tuple(5, "foo"));
        }

        // Create needed Runnables
        RunnableReadAll rTry = new RunnableReadAll(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 100, 1, verbose);
        RunnableReadAll rTry2 = new RunnableReadAll(this.linda, new Tuple(Integer.class, String.class), 2, 2, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTry);
        Thread t2 = new Thread(rFill);
        Thread t3 = new Thread(rTry2);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }
        // Wait for the fill to be end to launch the second tryTake
        t3.start();
        t3.join();

        List<Tuple> tupleList = new ArrayList<>();
        tupleList.add(new Tuple(4, "foo"));
        tupleList.add(new Tuple(5, "foo"));
        // Verify properties
        assertEquals(fill, linda.getTuples());
        assertEquals(new LinkedList<Tuple>(), rTry.getResultList());
        assertEquals(tupleList, rTry2.getResultList());
    }

    public void testEventRegisterTakeImmediate() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("CB Take Immediate test, tuple already in the linda: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableCB rTake = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 20, 2, verbose, Linda.eventMode.TAKE, Linda.eventTiming.IMMEDIATE, false);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertEquals(new Tuple(4, "foo"), rTake.getResultList().get(0));
    }

    public void testEventRegisterTakeImmediateTupleNotAlreadyPresent() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("CB Take Immediate test, tuple not already in the linda: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableCB rTake = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 0, 2, verbose, Linda.eventMode.TAKE, Linda.eventTiming.IMMEDIATE, false);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 100, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertEquals(new Tuple(4, "foo"), rTake.getResultList().get(0));
    }

    public void testEventRegisterTakeFuture() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("CB Take Future test, tuple already in the linda, add a new one, verify it takes the newly added: ");
        }

        List<Tuple> fill = new LinkedList<>();
        List<Tuple> fill2 = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
            fill2.add(new Tuple(5, "foo"));
        }

        // Create needed Runnables
        RunnableCB rTake = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 100, 2, verbose, Linda.eventMode.TAKE, Linda.eventTiming.FUTURE, false);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);
        Runnable rFill2 = new RunnableFillLinda(this.linda, fill2, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);
        Thread t3 = new Thread(rFill2);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        t3.start();
        t3.join();

        // Verify properties
        assertEquals(new Tuple(5, "foo"), rTake.getResultList().get(0));
        assertEquals("Message list should contains [4 \"foo\"]", fill, linda.getTuples());
    }

    public void testEventRegisterTakeImmediateReinscription() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("CB Take Immediate test and reinscription, tuple already in the linda: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
            fill.add(new Tuple(5, "foo"));
        }

        // Create needed Runnables
        RunnableCB rTake = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 20, 2, verbose, Linda.eventMode.TAKE, Linda.eventTiming.IMMEDIATE, true);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertEquals(new Tuple(4, "foo"), rTake.getResultList().get(0));
        assertEquals(new Tuple(5, "foo"), rTake.getResultList().get(1));
    }

    public void testEventRegisterReadImmediate() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("CB Read Immediate test, tuple already in the linda: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableCB rRead = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 20, 2, verbose, Linda.eventMode.READ, Linda.eventTiming.IMMEDIATE, false);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rRead);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertEquals(new Tuple(4, "foo"), rRead.getResultList().get(0));
    }

    public void testEventRegisterReadImmediateTupleNotAlreadyPresent() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("CB Read Immediate test, tuple not already in the linda: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableCB rRead = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 0, 2, verbose, Linda.eventMode.READ, Linda.eventTiming.IMMEDIATE, false);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 100, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rRead);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertEquals(new Tuple(4, "foo"), rRead.getResultList().get(0));
    }

    public void testEventRegisterReadFuture() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("CB Read Future test, tuple already in the linda, add a new one, verify it Reads the newly added: ");
        }

        List<Tuple> fill = new LinkedList<>();
        List<Tuple> fill2 = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
            fill2.add(new Tuple(5, "foo"));
        }

        // Create needed Runnables
        RunnableCB rRead = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 100, 2, verbose, Linda.eventMode.READ, Linda.eventTiming.FUTURE, false);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);
        Runnable rFill2 = new RunnableFillLinda(this.linda, fill2, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rRead);
        Thread t2 = new Thread(rFill);
        Thread t3 = new Thread(rFill2);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        t3.start();
        t3.join();

        fill.addAll(fill2);

        // Verify properties
        assertEquals(new Tuple(5, "foo"), rRead.getResultList().get(0));
        assertEquals("Message list should contains [4 \"foo\"] [5 \"foo\"]", fill, linda.getTuples());
    }

    public void testEventRegisterReadImmediateDouble() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("CB Read Immediate test with two read CB: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
        }

        // Create needed Runnables
        RunnableCB rRead = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 0, 2, verbose, Linda.eventMode.READ, Linda.eventTiming.IMMEDIATE, false);
        RunnableCB rRead1 = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 0, 2, verbose, Linda.eventMode.READ, Linda.eventTiming.IMMEDIATE, false);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rRead);
        Thread t12 = new Thread(rRead1);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t12.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t12.join();
            t2.join();
        }

        // Verify properties
        assertEquals(new Tuple(4, "foo"), rRead.getResultList().get(0));
        assertEquals(new Tuple(4, "foo"), rRead1.getResultList().get(0));
    }

    public void testEventRegisterTakeImmediateAndSimpleTake() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("CB Take Immediate test + simple Take, tuple already in the linda: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
            fill.add(new Tuple(5, "foo"));
        }

        // Create needed Runnables
        RunnableCB rTake = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 20, 2, verbose, Linda.eventMode.TAKE, Linda.eventTiming.IMMEDIATE, false);
        RunnableTake rTake1 = new RunnableTake(this.linda, new Tuple(Integer.class, String.class), 25, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t12 = new Thread(rTake1);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t12.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t12.join();
            t2.join();
        }

        // Verify properties
        assertEquals(new Tuple(4, "foo"), rTake.getResultList().get(0));
        assertEquals(new Tuple(5, "foo"), rTake1.getResultList().get(0));
    }

    public void testEventRegisterTakeFuturePlusTake() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("CB Take Future test + Take simple, tuple already in the linda, add a new one, verify it takes the newly added: ");
        }

        List<Tuple> fill = new LinkedList<>();
        List<Tuple> fill2 = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
            fill2.add(new Tuple(5, "foo"));
        }

        // Create needed Runnables
        RunnableCB rTake = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 100, 2, verbose, Linda.eventMode.TAKE, Linda.eventTiming.FUTURE, false);
        RunnableTake rTake1 = new RunnableTake(this.linda, new Tuple(Integer.class, String.class), 25, 2, verbose);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);
        Runnable rFill2 = new RunnableFillLinda(this.linda, fill2, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rTake);
        Thread t12 = new Thread(rTake1);
        Thread t2 = new Thread(rFill);
        Thread t3 = new Thread(rFill2);

        // Start the threads
        {
            t1.start();
            t12.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t12.join();
            t2.join();
        }

        t3.start();
        t3.join();

        // Verify properties
        assertEquals(new Tuple(5, "foo"), rTake.getResultList().get(0));
        assertEquals(new Tuple(4, "foo"), rTake1.getResultList().get(0));
        assertEquals("Message list should contains nothing", new LinkedList<Tuple>(), linda.getTuples());
    }

    // Réinscrire du read est une mauvaise idée, on fait exploser notre pile d'appel
    /*public void testEventRegisterReadImmediateReinscription() throws InterruptedException {
        if (verbose) {
            Logger.logInfo("CB Read Immediate test and reinscription, tuple already in the linda: ");
        }

        List<Tuple> fill = new LinkedList<>();

        // Create fill list to fill the linda server
        {
            fill.add(new Tuple(4, "foo"));
            fill.add(new Tuple(5, "foo"));
        }

        // Create needed Runnables
        RunnableCB rRead = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 20, 2, verbose, Linda.eventMode.READ, Linda.eventTiming.IMMEDIATE, true);
        Runnable rFill = new RunnableFillLinda(this.linda, fill, 0, 1, verbose);

        // Create needed Threads
        Thread t1 = new Thread(rRead);
        Thread t2 = new Thread(rFill);

        // Start the threads
        {
            t1.start();
            t2.start();
        }

        // Wait for the threads to finish
        {
            t1.join();
            t2.join();
        }

        // Verify properties
        assertEquals(new Tuple(4, "foo"), rRead.getResultList().get(0));
        assertEquals(new Tuple(4, "foo"), rRead.getResultList().get(1));
    }*/

    // Faire les tests de save
    public void testSave() throws Exception {
        this.linda.write(new Tuple(0, "foo"));
        RunnableCB rTake = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 0, 2, verbose, Linda.eventMode.TAKE, Linda.eventTiming.FUTURE, false);
        Thread t1 = new Thread(rTake);
        t1.start();
        t1.join();
        this.linda.write(new Tuple(0, 1));

        Thread.sleep(2 * SAVING_TIME);

        ServerState state = (ServerState) DataSave.loadData("./saves/serverState.save");

        List<Tuple> tuples = new LinkedList<>();
        tuples.add(new Tuple(0, "foo"));
        tuples.add(new Tuple(0, 1));
        assertEquals(tuples, state.getTuples());
        List<Tuple> callbacks = (List<Tuple>) state.getCallbacks();
        assertEquals(Linda.eventTiming.FUTURE, callbacks.get(0).get(1));
        assertEquals(Linda.eventMode.TAKE, callbacks.get(0).get(0));
        assertEquals(new Tuple(Integer.class, String.class), callbacks.get(0).get(2));
    }

    public void testRecup() throws Exception {
        this.linda.write(new Tuple(0, "foo"));
        RunnableCB rTake = new RunnableCB(this.linda, new Tuple(Integer.class, String.class), 0, 2, verbose, Linda.eventMode.TAKE, Linda.eventTiming.FUTURE, false);
        Thread t1 = new Thread(rTake);
        t1.start();
        t1.join();
        this.linda.write(new Tuple(0, 1));

        LindaServer.kill();
        LindaServer.main(new String[]{"true"});

        this.linda.write(new Tuple(0, "bar"));

        List<Tuple> tuples = new LinkedList<>();
        tuples.add(new Tuple(0, "foo"));
        tuples.add(new Tuple(0, 1));
        assertEquals(tuples, this.linda.getTuples());
        tuples = new LinkedList<>();
        tuples.add(new Tuple(0, "bar"));
        assertEquals(tuples, rTake.getResultList());
    }
}