package linda.shm;

import java.util.Collection;
import java.util.List;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

import linda.Callback;
import linda.Couple;
import linda.Linda;
import linda.Tuple;

/** Shared memory implementation of Linda. */
public class CentralizedLinda implements Linda {
	
	// Problème de synchro => take et try take en même temps => Comment on fait ?
	
	/* 
	 * Using Semaphore to wait for a message when not present in message
	 * Using Collection of Tuple<Template, Object> that will contains the queue (allows to avoid starvation between callback and synchronous wait)
	 * Choice is to feed first the read queue then the take queue
	 */
	private final Collection<Couple<Tuple, Object>> queueRead;
	private final Collection<Couple<Tuple, Object>> queueTake;
	
	// A list so messages will be fifo
	private final List<Tuple> messages;
	
    public CentralizedLinda() {
    	// Using Set implies no order in the queue
    	this.queueRead = new LinkedList<>();
    	this.queueTake = new LinkedList<>();
    	this.messages = new LinkedList<>();
    }

	@Override
	public void write(Tuple t) {
		Tuple t_cloned = t.deepclone();
		synchronized (this.messages) {
			this.messages.add(t_cloned);
		}
		
		// Feed all the readers
		Collection<Couple<Tuple,Object>> toRemove = new LinkedList<>();
		synchronized (this.queueRead) {
			for (Couple<Tuple,Object> c: this.queueRead) {
				if(t.matches(c.getFirst())) {
					toRemove.add(c);
				}
			}
			this.queueRead.removeAll(toRemove);
		}
		for (Couple<Tuple,Object> c: toRemove) {
			this.waitingCall(c.getSecond(), t.deepclone(), false);
		}
		
		Couple<Tuple,Object> toRemoveTake = null;
		// Feed one taker
		synchronized (this.queueTake) {
			for (Couple<Tuple,Object> c: this.queueTake) {
				if(t.matches(c.getFirst())) {
					// Since their is only one object that will be removed, we can do it here
					toRemoveTake = c;
					break;
				}
			}
			this.queueTake.remove(toRemoveTake);
		}
		if (toRemoveTake != null) {
			this.waitingCall(toRemoveTake.getSecond(), t_cloned, true);
		}
	}

	@Override
	public Tuple take(Tuple template) {
			Tuple result = this.blockingSearch(template.deepclone(), this.queueTake);
			synchronized (this.messages) {
				this.messages.remove(result);
			}
			return result;
	}

	@Override
	public Tuple read(Tuple template) {
		return this.blockingSearch(template.deepclone(), this.queueRead).deepclone();
	}

	@Override
	public Tuple tryTake(Tuple template) {
		Tuple result = this.searchTuple(template.deepclone());
		if (result != null) {
			synchronized (this.messages) {
				this.messages.remove(result);
			}
		}
		return result;
	}

	@Override
	public Tuple tryRead(Tuple template) {
		return this.searchTuple(template.deepclone());
	}

	@Override
	public Collection<Tuple> takeAll(Tuple template) {
		// Iterator have a remove method => Can be used if we do not factorize
		// For the take all, the first that go with the template will take all 
		// and if an other come with the same template he will have nothing
		Collection<Tuple> tuples;
		synchronized(this.messages) {
			// Look for matching tuples
			tuples = this.searchAllTuple(template.deepclone());
			// Remove matching tuples from the messages list
			for(Tuple t: tuples) {
					this.messages.remove(t);
			}
		}
		return tuples;
	}

	@Override
	public Collection<Tuple> readAll(Tuple template) {
		synchronized (this.messages) {
			return this.searchAllTuple(template.deepclone());
		}
	}

	@Override
	public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
		/* 2 mode : TAKE, READ
		 * 2 timing : IMMEDIATE, FUTURE
		 */
		if (timing == eventTiming.IMMEDIATE) {
			this.eventImmediate(mode, template.deepclone(), callback);
		}
		else {
			if (mode == eventMode.READ) {
				synchronized (this.queueRead) {
					this.queueRead.add(new Couple<>(template.deepclone(), callback));
				}
			}
			else {
				synchronized (this.queueTake) {
					this.queueTake.add(new Couple<>(template.deepclone(), callback));
				}
			}
			
		}		
	}

	@Override
	public void debug(String prefix) {
		//System.out.println(prefix + this.queueRead);
	}

	/**
	 * Function that return a copy of the Tuple of the centralized Linda
	 * @return a List of tuples representing the tuple in memory
	 */
	public List<Tuple> getTuples(){
		synchronized (this.messages) {
			return this.messages.stream().map(Tuple::deepclone).collect(Collectors.toList());
		}
	}

// ---------------------------------------------------------------- //
	
	
	/**
	 * Return the first template that match the template in a non blocking way
	 * @param template The template to be matched
	 * @return The first message that match the template or null if none correspond
	 */
	private Tuple searchTuple(Tuple template) {
		synchronized (this.messages) {
			for (Tuple t : this.messages) {
				if (t.matches(template)){
					return t.deepclone();
				}
			}
			return null;
		}
	}
	
	/**
	 * Return a collection containing all the tuples that match the template
	 * @param template the template to be match
	 * @return A collection containing the tuples that matches or an empty collection if none matches
	 */
	private Collection<Tuple> searchAllTuple(Tuple template) {
		Collection<Tuple> tuples = new LinkedList<>();
		for(Tuple t: this.messages) {
			if (t.matches(template)) {
				tuples.add(t.deepclone());
			}
		}
		return tuples;
	}
	
	/**
	 * Same as the searchTuple but blocks if no elements found and wait for an element to be write
	 * @param template the template to match
	 * @param waitingList The waiting list in which we wait
	 * @return A tuple corresponding to the template
	 */
	private Tuple blockingSearch(Tuple template, Collection<Couple<Tuple,Object>> waitingList) {
		Tuple result = this.searchTuple(template);
		while (result == null) {
			Semaphore waitingForTuple = new Semaphore(0);
			Couple<Tuple, Object> waitingCouple = new Couple<>(template, waitingForTuple);
			synchronized (waitingList) {
				waitingList.add(waitingCouple);
			}
			try {
				waitingForTuple.acquire();
			} catch (InterruptedException e) {
				//If the acquire fails, do nothing, search again and if the Tuple is not here, wait again
				synchronized (waitingList) {
					waitingList.remove(waitingCouple);
				}
			}
			result = this.searchTuple(template);
		}
		return result;
	}
	
	/**
	 * Compute the callback when the eventTiming is immediate
	 * @param mode The event mode
	 * @param template the template to match
	 * @param cb the callback to call
	 */
	private void eventImmediate(eventMode mode, Tuple template, Callback cb) {
		Tuple t;
		Collection<Couple<Tuple, Object>> queue;
		if (mode == eventMode.READ){
			t = this.tryRead(template);
			queue = this.queueRead;
		}
		else {
			t = this.tryTake(template);
			queue = this.queueTake;
		}
		if (t != null) {
			cb.call(t);
		}
		else {
			synchronized (queue) {
				queue.add(new Couple<>(template, cb));
			}
		}
	}
	
	/**
	 * Verify if the object is a Semaphore or a callback and do the appropriate actions
	 * @param waiter the object that wait for a resource
	 * @param tuple the matching tuple
	 * @param removeTuple Says if we have to remove the tuple from the tuple space
	 */
	private void waitingCall(Object waiter, Tuple tuple, boolean removeTuple) {
		if (waiter instanceof Semaphore) {
			// Case Semaphore
			((Semaphore)waiter).release();
		} else {
			if (removeTuple) {
				synchronized (this.messages) {
					this.messages.remove(tuple);
				}
			}
			((Callback)waiter).call(tuple);
		}
	}

}
