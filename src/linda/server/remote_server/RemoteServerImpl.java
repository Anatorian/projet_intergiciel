package linda.server.remote_server;

import linda.Linda.eventMode;
import linda.Linda.eventTiming;
import linda.Tuple;
import linda.server.miscellaneous.DataSave;
import linda.server.miscellaneous.ServerState;
import linda.server.remote_callback.EncapsulatedCallback;
import linda.server.remote_callback.RemoteCallback;
import linda.shm.CentralizedLinda;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static linda.server.remote_server.Logger.logInfo;
import static linda.server.remote_server.Logger.logWarn;

public class RemoteServerImpl extends UnicastRemoteObject implements RemoteServer {

    private static final long serialVersionUID = 1L;
    private List<Tuple> callbacks;
    private CentralizedLinda linda;
    private ServerState serverState;


    public RemoteServerImpl() throws RemoteException {
        this(true);
    }

    public RemoteServerImpl(boolean loadState) throws RemoteException {
        super();
        // Create an empty server state
        ServerState st = new ServerState(new LinkedList<>(), new LinkedList<>());
        if (loadState) {
            try {
                // Try to find a local server state
                st = RemoteServer.getLocalServerState();
            } catch (IOException e) {
                logWarn("No save found, booting from an empty state");
            }
        }
        initialize_server(loadState, st);
    }

    public RemoteServerImpl(ServerState state) throws RemoteException {
        super();
        initialize_server(true, state);
    }

    @Override
    public void write(Tuple t) {
        this.linda.write(t);
    }

    @Override
    public Tuple take(Tuple template) {
        return this.linda.take(template);
    }

    @Override
    public Tuple read(Tuple template) {
        return this.linda.read(template);
    }

    @Override
    public Tuple tryTake(Tuple template) {
        return this.linda.tryTake(template);
    }

    @Override
    public Tuple tryRead(Tuple template) {
        return this.linda.tryRead(template);
    }

    @Override
    public Collection<Tuple> takeAll(Tuple template) {
        return this.linda.takeAll(template);
    }

    @Override
    public Collection<Tuple> readAll(Tuple template) {
        return this.linda.readAll(template);
    }

    @Override
    public void eventRegister(eventMode mode, eventTiming timing, Tuple template, RemoteCallback callback) {
        EncapsulatedCallback cb = new EncapsulatedCallback(callback, this);
        this.callbacks.add(new Tuple(mode, timing, template, callback));
        this.linda.eventRegister(mode, timing, template, cb);
    }

    @Override
    public void debug(String prefix) {
        this.linda.debug(prefix);
        
    }

    @Override
    public void saveState() {

        this.serverState = new ServerState(this.linda.getTuples(), this.callbacks);
        try {
            DataSave.saveData(this.save_folder + "/serverState.save", this.serverState);
        } catch (IOException e) {
            logWarn("Error saving server state");
        }
    }

    @Override
    public void recoverState() throws RemoteException {
        // Recover server state
        logInfo("Launching the server");
        // Write each Tuple to the Centralized Linda
        for (Tuple t : this.serverState.getTuples()) {
            this.write(t);
        }

        // Register callbacks
        for (Tuple cb : this.serverState.getCallbacks()) {
            this.eventRegister((eventMode) cb.get(0), (eventTiming) cb.get(1), (Tuple) cb.get(2), ((RemoteCallback) cb.get(3)));
        }
    }

    @Override
    public void removeCallback(RemoteCallback cb) {
        this.callbacks.remove(cb);
    }

    @Override
    public ServerState getServerState() {
        return this.serverState;
    }

    public List<Tuple> getTuples() {
        return this.linda.getTuples();
    }

    private void initialize_server(boolean loadState, ServerState state) throws RemoteException {
        this.callbacks = new LinkedList<>();
        this.linda = new CentralizedLinda();
        this.serverState = state;
        if (loadState) {
            this.recoverState();
        }
    }

}
