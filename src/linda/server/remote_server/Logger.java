package linda.server.remote_server;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {

    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_PURPLE = "\u001B[35m";
    private static final String ANSI_CYAN = "\u001B[36m";

    public Logger() {

    }

    public static void logErr(String log) {
        System.out.println(ANSI_RED + getFormattedDate(LocalDateTime.now()) + ": [ERROR] " + log + ANSI_RESET);
    }

    public static void logWarn(String log) {
        System.out.println(ANSI_YELLOW + getFormattedDate(LocalDateTime.now()) + ": [WARNING] " + log + ANSI_RESET);
    }

    public static void logInfo(String log) {
        System.out.println(ANSI_GREEN + getFormattedDate(LocalDateTime.now()) + ": [INFO] " + log + ANSI_RESET);
    }

    private static String getFormattedDate(LocalDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return date.format(formatter);
    }
}
