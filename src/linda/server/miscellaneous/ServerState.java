package linda.server.miscellaneous;

import linda.Tuple;

import java.io.Serializable;
import java.util.Collection;

public class ServerState implements Serializable {

    private Collection<Tuple> tuples;
    // Will be better to implement a class that correspond to a quartet
    // Tuple[EventMode, EventTiming, Tuple, RemoteCallback
    private Collection<Tuple> callbacks;

    public ServerState(Collection<Tuple> tuples, Collection<Tuple> callbacks) {
        this.tuples = tuples;
        this.callbacks = callbacks;
    }

    public Collection<Tuple> getCallbacks() {
        return callbacks;
    }

    public Collection<Tuple> getTuples() {
        return tuples;
    }
}
