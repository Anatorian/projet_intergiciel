package linda.server.miscellaneous;

import java.io.*;

public class DataSave {

    public static Object loadData(String path) throws IOException {
        Object returnValue = null;
        try {
        FileInputStream fileIn = new FileInputStream(path);
        ObjectInputStream objectIn = new ObjectInputStream(fileIn);
        returnValue = objectIn.readObject();
        objectIn.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    public static void saveData(String path, Object toSave) throws IOException{
        FileOutputStream fileOutputStream = new FileOutputStream(path, false);
        ObjectOutputStream objectOut = new ObjectOutputStream(fileOutputStream);
        objectOut.writeObject(toSave);
        objectOut.close();
    }

}
