package linda.server;

import linda.server.miscellaneous.ServerState;
import linda.server.remote_server.Logger;
import linda.server.remote_server.RemoteServer;
import linda.server.remote_server.RemoteServerImpl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.LinkedList;

import static linda.server.remote_server.Logger.logInfo;
import static linda.server.remote_server.Logger.logWarn;

public class LindaServer {

    public final static int PORT = 4000;
    public final static String HOST = "localhost";
    public final static String LINDA_NAME = "LindaServer";
    public final static int SAVING_TIME = 500;
    private final static String PRIMARY_URI = "rmi://" + HOST + ":" + PORT + "/" + LINDA_NAME;

    public static void kill() {
        try {
            Naming.unbind("rmi://" + HOST + ":" + PORT + "/" + LINDA_NAME);
        } catch (RemoteException | MalformedURLException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    private static void autosave(RemoteServer remoteLinda) {
        Runnable autoSaveState = () -> {
            while (true) {
                try {
                    remoteLinda.saveState();
                } catch (RemoteException e) {
                    logWarn("Error saving server state");
                }
                try {
                    Thread.sleep(SAVING_TIME);
                } catch (InterruptedException e) {
                    // Do nothing
                }
            }
        };
        // Loop to save the server state regularly
        Thread tSave = new Thread(autoSaveState);
        tSave.start();
    }

    private static void createNameServer() throws RemoteException {
        try {
            LocateRegistry.createRegistry(PORT);
        } catch (java.rmi.server.ExportException e) {
            logInfo("A registry is already running, proceeding...");
        }
    }

    public static void main(String[] args) throws Exception {

        // Program variables
        boolean isPrimary = false;
        boolean isPrimaryUp = false;
        ServerState serverState = new ServerState(new LinkedList<>(), new LinkedList<>());
        RemoteServer primary = null;

        // Set the load state variable
        if (args.length == 1) {
            if (args[0].equals("true")) {
                try {
                    // Try to find a local server state
                    serverState = RemoteServer.getLocalServerState();
                } catch (IOException e) {
                    Logger.logWarn("No save found.");
                }
            }
        }

        while (!isPrimary) {
            // Création du serveur de noms
            //System.setProperty("java.rmi.server.hostname", "192.168.43.206");
            createNameServer();

            // Look for a primary server
            try {
                primary = (RemoteServer) Naming.lookup(PRIMARY_URI);
                // A primary is found
                logInfo("A primary server is already running. Waiting to the primary to be down to start.");
                isPrimaryUp = true;
            } catch (NotBoundException e) {
                // No primary found
                logInfo("No primary server found. Start as a primary");
                isPrimary = true;
            } catch (MalformedURLException | RemoteException e) {
                e.printStackTrace();
            }

            // if primary, launch the server
            if (isPrimary) {
                //  Création de l'objet Linda,
                //  et enregistrement du linda dans le serveur de nom
                RemoteServer remoteLinda = new RemoteServerImpl(serverState);

                try {
                    Naming.bind(PRIMARY_URI, remoteLinda);
                    // Service prêt : attente d'appels
                    logInfo("System is up and running");
                    autosave(remoteLinda);
                } catch (AlreadyBoundException e) {
                    // Thinking we were primary but seems an other backup kicks in faster
                    logWarn("Wanted to start as a primary server but seems that an other backup starts faster.");
                    isPrimary = false;
                    isPrimaryUp = true;
                } catch (MalformedURLException | RemoteException e) {
                    e.printStackTrace();
                }
            }
            // Else wait for the primary to fall
            else {
                while (isPrimaryUp) {
                    try {
                        // Get a backup of the server state
                        assert primary != null;
                        serverState = primary.getServerState();

                        // Wait a certain amount of time at greater or equals to the saving time
                        Thread.sleep(SAVING_TIME);
                    } catch (RemoteException e) {
                        logWarn("The primary has failed. Trying to start as a primary");
                        isPrimaryUp = false;
                    }
                }
            }
        }
        logInfo("Server started at: " + PRIMARY_URI);
    }
}
