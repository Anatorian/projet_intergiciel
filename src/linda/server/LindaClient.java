package linda.server;

import linda.Callback;
import linda.Linda;
import linda.Tuple;
import linda.server.remote_callback.RemoteCallbackImpl;
import linda.server.remote_server.RemoteServer;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.List;

import static linda.server.remote_server.Logger.*;

/**
 * Client part of a client/server implementation of Linda.
 * It implements the Linda interface and propagates everything to the server it is connected to.
 */
public class LindaClient implements Linda {

	private static final int MAX_RETRY = 3;
	private static final int RETRY_INTERVAL = 500;

	private RemoteServer linda;
	private String serverURI;

	/** Initializes the Linda implementation.
	 *  @param serverURI the URI of the server, e.g. "rmi://localhost:4000/LindaServer" or "//localhost:4000/LindaServer".
	 */
	public LindaClient(String serverURI) {
		//  Connexion au serveur de noms (obtention d'un handle)
		this.serverURI = serverURI;
		tryGetServer();
	}

	@Override
	public void write(Tuple t) {
		try {
			this.linda.write(t);
		} catch (RemoteException e) {
			logWarn("Error writing tuple: " + t);
			tryGetServer();
			this.write(t);
		}
	}

	@Override
	public Tuple take(Tuple template) {
		try {
			return this.linda.take(template);
		} catch (RemoteException e) {
			logWarn("Error take with template: " + template);
			tryGetServer();
			return this.take(template);
		}
	}

	@Override
	public Tuple read(Tuple template) {
		try {
			return this.linda.read(template);
		} catch (RemoteException e) {
			logWarn("Error read with template: " + template);
			tryGetServer();
			return this.read(template);
		}
	}

	@Override
	public Tuple tryTake(Tuple template) {
		try {
			return this.linda.tryTake(template);
		} catch (RemoteException e) {
			logWarn("Error tryTake with template: " + template);
			tryGetServer();
			return this.tryTake(template);
		}
	}

	@Override
	public Tuple tryRead(Tuple template) {
		try {
			return this.linda.tryRead(template);
		} catch (RemoteException e) {
			logWarn("Error tryRead with template: " + template);
			tryGetServer();
			return this.tryRead(template);
		}
	}

	@Override
	public Collection<Tuple> takeAll(Tuple template) {
		try {
			return this.linda.takeAll(template);
		} catch (RemoteException e) {
			logWarn("Error takeAll with template: " + template);
			tryGetServer();
			return this.takeAll(template);
		}
	}

	@Override
	public Collection<Tuple> readAll(Tuple template) {
		try {
			return this.linda.readAll(template);
		} catch (RemoteException e) {
			logWarn("Error readAll with template: " + template);
			tryGetServer();
			return this.readAll(template);
		}
	}

	@Override
	public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
		try {
			this.linda.eventRegister(mode, timing, template, new RemoteCallbackImpl(callback));
		} catch (RemoteException e) {
			logWarn("Error eventRegister");
			tryGetServer();
			this.eventRegister(mode, timing, template, callback);
		}
		
	}

	@Override
	public void debug(String prefix) {
		try {
			this.linda.debug(prefix);
		} catch (RemoteException e) {
			logWarn("Error debug");
			tryGetServer();
			this.debug(prefix);
		}
		
	}

	public List<Tuple> getTuples() {
		try {
			return this.linda.getTuples();
		} catch (RemoteException e) {
			logWarn("Error getTuples");
			tryGetServer();
			return this.getTuples();
		}
	}

	private void tryGetServer() {
		int retry = MAX_RETRY;
		while (retry > 0) {
			try {
				this.linda = (RemoteServer) Naming.lookup(serverURI);
				logInfo("Server successfully found");
				break;
			} catch (MalformedURLException | RemoteException | NotBoundException e) {
				retry--;
				logWarn("Can't access remote server. Remaining retry: " + retry);
				try {
					Thread.sleep(RETRY_INTERVAL);
				} catch (InterruptedException ex) {
					logWarn("Sleeping time interrupted");
				}
			}
		}
		if (retry == 0) {
			logErr("Can't access remote server. Client going to be shut down.");
			System.exit(1);
		}
	}

}
