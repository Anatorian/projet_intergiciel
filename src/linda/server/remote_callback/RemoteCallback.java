package linda.server.remote_callback;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

import linda.Tuple;

public interface RemoteCallback extends Remote, Serializable {
	
	void call(Tuple t) throws RemoteException;

}
