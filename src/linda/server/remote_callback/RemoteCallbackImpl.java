package linda.server.remote_callback;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import linda.Callback;
import linda.Tuple;

public class RemoteCallbackImpl extends UnicastRemoteObject implements RemoteCallback{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4967429398414852785L;
	private Callback cb;
	
	public RemoteCallbackImpl(Callback cb) throws RemoteException {
		super();
		this.cb = cb;
	}

	@Override
	public void call(Tuple t) {
		this.cb.call(t);
	}

}
