package linda.server.remote_callback;

import linda.Callback;
import linda.Tuple;
import linda.server.remote_server.RemoteServer;

import java.rmi.RemoteException;

public class EncapsulatedCallback implements Callback {

    private RemoteCallback cb;
    private RemoteServer server;

    public EncapsulatedCallback(RemoteCallback cb) {
        this.cb = cb;
    }

	public EncapsulatedCallback(RemoteCallback cb, RemoteServer server) {
		this(cb);
		this.server = server;
	}

	@Override
	public void call(Tuple t) {
		try {
            this.server.removeCallback(cb);
            this.cb.call(t);
        } catch (RemoteException e) {
			// Do nothing, can't contact the caller
		}
	}

}
